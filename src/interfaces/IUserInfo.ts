// Interfaz para definir la estructura de un usuario
export interface IUserInfo {
  id: number;
  name: string;
  lastName: string;
  email: string;
  age: number;
}
